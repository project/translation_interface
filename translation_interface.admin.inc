<?php

function translation_interface_settings(&$form_state) {
  $form['info'] = array(
    '#title' => t('Translate Interface'),
    '#description' => t('Configure the scope of the translate interface.  The translation interface will only show content types which are enabled for translation for users with appropriate i18n_access permission to a language.'),
  );
  $form['translation_interface_views'] = array(
    '#title' => t('Enable interface for translating Views'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('translation_interface_views', 1),
  );
  $form['translation_interface_redirect'] = array(
    '#title' => t('Redirect permissioned translators to the translate page at login'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('translation_interface_redirect', 0),
  );
  $form['translation_interface_redirect_root'] = array(
    '#title' => t('Redirect the root user of the site at login'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('translation_interface_redirect_root', 0),
  );
  $form['translation_interface_only'] = array(
    '#title' => t('Hide other Drupal translation interfaces from translators'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('translation_interface_only', 0),
  );
  $form['#submit'][] = 'translation_interface_settings_submit';

  return system_settings_form($form);
}

function translation_interface_settings_submit($form, &$form_state) {
  variable_set('translation_interface_views', $form_state['values']['translation_interface_views']);
  variable_set('translation_interface_redirect', $form_state['values']['translation_interface_redirect']);
  variable_set('translation_interface_redirect_root', $form_state['values']['translation_interface_redirect_root']);
  variable_set('translation_interface_only', $form_state['values']['translation_interface_only']);
  drupal_flush_all_caches();
}

